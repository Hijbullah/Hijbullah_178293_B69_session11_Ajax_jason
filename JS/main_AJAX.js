/*
  Created by Hijbullah on 8/19/2017.
 */

$(document).ready(function () {
    $("#btn").click(function () {

        var myRequest = new XMLHttpRequest();

        myRequest.open('GET','https://learnwebcode.github.io/json-example/animals-1.json');

        myRequest.onload = function () {

            var myData = myRequest.responseText;

            $("#display").html(myData);
            $("#display").css("background","#ddd");
            $("#display").css("width","300px");
            $("#display").css("height","auto");
            $("#display").css("white-space","pre");
            $("#display").css("margin-top","20px");
            $("#display").css("padding","20px");
            $("#display").css("border-radius","5px");
            $("#display").css("transition",".7s");

        };

        myRequest.send();
    });
});
