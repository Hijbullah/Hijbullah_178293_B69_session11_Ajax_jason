/*
  Created by Hijbullah Aminb on 8/19/2017.
 */


/*object*/
/*var cat = {
    "name" : "MeosAlot",
    "species" : "Cat",
    "favfood" : "Tuna"
};

document.write("I'm " + cat.name + ", I'm a "+ cat.species + ". I like to eat "+ cat.favfood + "<br>");

/!*Array*!/

var animals = [

    "Cat", "Dog", "Bird"
];

document.write(animals[0] + "<br>");*/

var mankinds = [
    {
      "name" : "Hijbullah",
        "age" :  23,
        "foods" : {
            "likes" : ["Meat", "Chicken", "Rice", "Vorta"],
            "dislikes" : ["Fish", "Dal", "Milk"]
        }
    },
    {
        "name" : "Farjana",
        "age" : 22,
        "foods" : {
            "likes" : ["Fuchka", "Ice Cream", "Chicken Fry"],
            "dislikes" : ["Rice", "Dal", "Eggs"]

        }
    },
    {
        "name" : "Rashed",
        "age" : 25,
        "foods" : {
            "likes" : ["Chicken Fry", "Potato", "Beaf"],
            "dislikes" : ["Meat", "Fish", "Ice"]
        }
    }
];


for(i = 0; i < mankinds.length; i++){

    var myLife = "Name: " + mankinds[i].name + "<br>";
    myLife += "Age: " + mankinds[i].age + "<br>";
    myLife += "Foods: <br> &nbsp; &nbsp; Likes: "+ mankinds[i].foods.likes + "<br>";
    myLife += "&nbsp; &nbsp; Dislikes: "+ mankinds[i].foods.dislikes + "<br>";

    document.write(myLife + "<hr>");
};

